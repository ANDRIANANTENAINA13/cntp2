s=10;

rand("seed",s);

format("e",16);

n_data=[10:10:100];
    for n=n_data

        A=rand(n,n);

        for i=1:n

            for j=1:n

                if ((i ~= j) & (i-1 ~= j) & (i+1 ~= j))
                    A(i,j)=0;
                end
            end
        end

            [L,U]=factoLU(A)
        
         k=n/10;
        //Calcul de l'erreur avant
        ferr(k)=norm(A-L*U)/norm(A);

    end

plot(n_data, ferr),xlabel("Data"),ylabel("Erreur"),title("METHODE DE VALIDATION")
