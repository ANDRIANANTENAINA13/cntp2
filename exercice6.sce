function [x,relres, resvec, it]=jacobi(A, b, tol, nmaxit)

n=size(A);
XINIT= zeros(n);

resvec=zeros(nmaxit);

relres=norm(b-A*XINIT)/norm(b);

dg1=(1.)./diag(A);

it=0;

while (relres > tol) & (it < nmaxit)

    it =it+1;
    x=XINIT + dg1*(b-A*XINIT);
    XINIT=x;
    relres=norm(b-A*XINIT)/norm(b);
    resvec(it)=relres;

end

endfunction

function [x,relres, resvec, it]=gaussseidel(A, b, tol, nmaxit)

n=size(A);
XINIT= zeros(n);

resvec=zeros(nmaxit);

relres=norm(b-A*XINIT)/norm(b);

while (relres > tol) & (it < nmaxit)

    it =it+1;
    x=tril(A)\(b-A*XINIT);
    x=x+XINIT;
    relres=norm(b-A*XINIT)/norm(b);
    resvec(it)=relres;

end

endfunction
